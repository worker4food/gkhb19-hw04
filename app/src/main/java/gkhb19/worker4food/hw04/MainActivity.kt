package gkhb19.worker4food.hw04

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import gkhb19.worker4food.hw04.adapters.PrimeAdapter
import gkhb19.worker4food.hw04.models.PrimeModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val model by viewModels<PrimeModel>()

    private var lastKnownSize = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startBtn.setOnClickListener { model.start() }
        stopBtn.setOnClickListener { model.stop() }
        clearBtn.setOnClickListener { model.clear() }

        model.isStarted.observe(this, Observer { isStarted ->
            startBtn.isEnabled = !isStarted
            stopBtn.isEnabled = isStarted
            clearBtn.isEnabled = !isStarted
        })

        primes.adapter = PrimeAdapter(model)

        model.size.observe(this, Observer { newSize ->
            val diff = newSize - lastKnownSize

            with(primes.adapter!!) {
                if (diff > 0)
                    notifyItemRangeInserted(0, diff)
                else
                    notifyItemRangeRemoved(newSize, -diff)
            }

            lastKnownSize = newSize
        })
    }
}
