package gkhb19.worker4food.hw04.adapters

import android.view.*
import androidx.recyclerview.widget.RecyclerView
import gkhb19.worker4food.hw04.R
import gkhb19.worker4food.hw04.models.*
import kotlinx.android.synthetic.main.list_item.view.*

class PrimeAdapter(private val model: PrimeModel) : RecyclerView.Adapter<PrimeAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)
            .run(::Holder)

    override fun getItemCount(): Int = model.size.value!!

    override fun onBindViewHolder(holder: Holder, position: Int) =
        holder.bind(model.primes.value!!.elementAt(position))

    inner class Holder(v: View) : RecyclerView.ViewHolder(v) {
        fun bind(number: Long) {
            itemView.primeText.text = "$number"
        }
    }
}
