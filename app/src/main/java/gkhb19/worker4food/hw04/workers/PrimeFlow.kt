package gkhb19.worker4food.hw04.workers

import kotlinx.coroutines.flow.*

fun isPrime (x: Long): Boolean = (2L until x).all { x.rem(it) != 0L }

fun primesFrom(seed: Long) = flow {
    generateSequence(seed, Long::inc)
        .filter(::isPrime)
        .forEach { emit(it) }
}
