package gkhb19.worker4food.hw04.models

import android.app.Application
import androidx.lifecycle.*
import gkhb19.worker4food.hw04.*
import gkhb19.worker4food.hw04.workers.*
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.*

class PrimeModel(app: Application) : AndroidViewModel(app) {

    private var _primesJob = MutableLiveData<Job>()

    val primes = MutableLiveData<Collection<Long>>().apply {
        value = listOf()
    }

    val size: LiveData<Int> = Transformations.map(primes) { it.size }

    val isStarted = Transformations.map(_primesJob) { it?.isActive ?: false }

    fun start() {
        val seed = primes.value?.firstOrNull()?.inc() ?: 2L

        _primesJob.value = viewModelScope.launch {

            primesFrom(seed).collect {
                primes.postValue(listOf(it) + primes.value!!)

                delay(TASK_MIN_TIME)
            }
        }
    }

    fun stop() {
        _primesJob.value?.cancel()
        _primesJob.value = null
    }

    fun clear() {
        primes.value = listOf()
    }
}
