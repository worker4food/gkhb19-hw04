# Geekhub 2019 - android

## Homework 04

### Branches
 * [via-workmanager][1] - with WorkManager
 * [via-flow][2] - with kotlin Flow

[Downloads](https://bitbucket.org/worker4food/gkhb19-hw04/downloads/)

[1]: ../via-workmanager
[2]: ../via-flow
